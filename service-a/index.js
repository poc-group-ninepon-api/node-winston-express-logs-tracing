const express = require('express');
const winston = require('winston');
const { initTracer } = require('jaeger-client');
const { default: axios } = require('axios');

// Set up Jaeger tracer
const config = {
  serviceName: 'ninepon-service-a',
  sampler: {
    type: 'const',
    param: 1,
  },
  reporter: {
    logSpans: true,
    agentHost: 'localhost',
    agentPort: 6832,
  }
};
const options = { logger: winston };
const tracer = initTracer(config, options);

// Set up Winston logger
const logger = winston.createLogger({
  level: 'info',
  format: winston.format.combine(
    winston.format.timestamp(),
    winston.format.json()
  ),
  transports: [ new winston.transports.Console() ]
});

// Middleware that adds a Jaeger span to each incoming request
const tracerMiddleware = (req, res, next) => {
  const span = tracer.startSpan(req.method + ' ' + req.originalUrl);
  span.log({ event: 'request_received' });
  req.span = span;
  next();
};

// Middleware that logs incoming requests and responses with span ID
const loggingMiddleware = (req, res, next) => {
  const spanId = req.span.context().toSpanId();
  const traceId = req.header('trace-id') || req.span.context().toTraceId();
  req.log = logger.child({ spanId, traceId });
  req.log.info(`Incoming request: ${req.method} ${req.originalUrl}`);
  res.on('finish', () => {
    // req.log.info(`Outgoing response: ${res.statusCode}`);
    req.span.log({ event: 'response_sent' });
    req.span.finish();
  });
  next();
};

// Route that logs a message
const exampleRoute = async (req, res) => {
  req.log.info('Hello, world!');
  const resB = await axios.get('http://localhost:4000/api', {
    headers:{ "trace-id": req.span.context().toTraceId() }
  } )
  req.log.info(resB.data)
  res.send('Hello, world! from service A');
};

// Set up the app
const app = express();
app.use(tracerMiddleware);
app.use(loggingMiddleware);
app.get('/api', exampleRoute);
app.listen(3000, () => {
  console.log('Server started on port 3000');
});