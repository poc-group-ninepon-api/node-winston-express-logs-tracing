const { NodeTracerProvider } = require('@opentelemetry/node');
const { SimpleSpanProcessor } = require('@opentelemetry/tracing');
const { JaegerExporter } = require('@opentelemetry/exporter-jaeger');

const { trace } = require('@opentelemetry/api');

const express = require('express');
const winston = require('winston');

const provider = new NodeTracerProvider({
  serviceName: 'express-service',
});
const exporter = new JaegerExporter({
  serviceName: 'express-service',
});
provider.addSpanProcessor(new SimpleSpanProcessor(exporter));
provider.register();

// ===== express ====
const app = express();
app.use((req, res, next) => {
  const span = trace.getTracer('express').startSpan(req.path);
//   trace.setSpanContext(span, trace.extract(trace.SpanContext, req.headers));
  req.span = span;
  res.on('finish', () => {
    span.setStatus(res.statusCode);
    span.end();
  });
  next();
});

app.get('/', (req, res) => {
    const span = req.span;
    span.addEvent('handling request');
    res.send('Hello World!');
});

app.listen(3000, () => {
    console.log('Server started on port 3000');
});