const express = require('express');
const winston = require('winston');
const { initTracer } = require('jaeger-client');

// Set up Jaeger tracer
const config = {
  serviceName: 'ninepon-service-b',
  sampler: { type: 'const', param: 1 },
  reporter: { logSpans: true }
};
const options = { logger: winston };
const tracer = initTracer(config, options);

// Set up Winston logger
const logger = winston.createLogger({
  level: 'info',
  format: winston.format.combine(
    winston.format.timestamp(),
    winston.format.json()
  ),
  transports: [ new winston.transports.Console() ]
});

// Middleware that adds a Jaeger span to each incoming request
const tracerMiddleware = (req, res, next) => {
  const span = tracer.startSpan(req.method + ' ' + req.originalUrl);
  span.log({ event: 'request_received' });
  req.span = span;
  next();
};

// Middleware that logs incoming requests and responses with span ID
const loggingMiddleware = (req, res, next) => {
  const spanId = req.span.context().toSpanId();
  let traceId = req.span.context().toTraceId();
  if(req.headers["trace-id"]){
    traceId = req.headers["trace-id"]
  }
  req.log = logger.child({ spanId, traceId });
  req.log.info(`Incoming request: ${req.method} ${req.originalUrl}`);
  res.on('finish', () => {
    // req.log.info(`Outgoing response: ${res.statusCode}`);
    req.span.log({ event: 'response_sent' });
    req.span.finish();
  });
  next();
};

// Set up the app
const app = express();
app.use(tracerMiddleware);
app.use(loggingMiddleware);

app.get('/api', (req, res) => {
  req.log.info('Hello, world!');
  res.send('Hello, world! from service B');
});

app.listen(4000, () => {
  console.log('Server started on port 4000');
});